﻿using System;
using System.Drawing;

namespace IJWTGH_Map_Creator
{
    public class Entity
    {
        private const float OriginSize = 0.1f;
        private readonly Brush _fillBrush = new SolidBrush(Color.FromArgb(50, Color.DodgerBlue));
        private readonly Brush _fillBrushSelected = new SolidBrush(Color.FromArgb(50, Color.OrangeRed));
        private readonly Pen _borderPen = new Pen(Color.FromArgb(50, Color.DodgerBlue), 1.5f);

        public string Name { get; set; }
        public Vector3 Location { get; set; }
        public Vector3 Size { get; set; }

        public Entity(string name, Vector3 location, Vector3 size)
        {
            Name = name;
            Location = location;
            Size = size;
        }

        public void Draw(Graphics g, bool selected = false)
        {
            RectangleF entityRect = new RectangleF((Location.X - Size.X / 2f) * Constants.MapScale,
                (Location.Z - Size.Z / 2f) * Constants.MapScale, Size.X * Constants.MapScale,
                Size.Z * Constants.MapScale).ChangeCoordSystem();
            // draw background
            g.FillRectangle(selected ? _fillBrushSelected : _fillBrush, entityRect);
            // draw border
            g.DrawRectangle(_borderPen, Rectangle.Round(entityRect));

            RectangleF entityOriginRect = new RectangleF((Location.X - OriginSize / 2f) * Constants.MapScale,
                (Location.Z - OriginSize / 2f) * Constants.MapScale, OriginSize * Constants.MapScale,
                OriginSize * Constants.MapScale).ChangeCoordSystem();
            // draw origin dot
            g.FillEllipse(Brushes.DodgerBlue, entityOriginRect);
        }

        public void TransformTo(float x, float y)
        {
            Location.TransformTo(x, y);
        }

        public bool ContainsPoint(Point p)
        {
            RectangleF entityRect = new RectangleF((Location.X - Size.X / 2f) * Constants.MapScale,
                (Location.Z - Size.Z / 2f) * Constants.MapScale, Size.X * Constants.MapScale,
                Size.Z * Constants.MapScale).ChangeCoordSystem();
            Region entityRegion = new Region(entityRect);
            // todo: union all regions.
            return entityRegion.IsVisible(p);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}