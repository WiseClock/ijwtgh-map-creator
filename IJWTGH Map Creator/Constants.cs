﻿namespace IJWTGH_Map_Creator
{
    public static class Constants
    {
        public static int MapScale = 50;
        public static int MapWidth = 100;
        public static int MapHeight = 100;
    }
}
