﻿using System.Windows.Forms;

namespace IJWTGH_Map_Creator
{
    public sealed class CanvasFrame : Panel
    {
        public CanvasFrame()
        {
            SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer |
                ControlStyles.OptimizedDoubleBuffer,
                true);
            UpdateStyles();
            DoubleBuffered = true;
        }
    }
}
