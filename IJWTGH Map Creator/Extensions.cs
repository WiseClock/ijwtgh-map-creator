﻿using System.Drawing;

namespace IJWTGH_Map_Creator
{
    public static class Extensions
    {
        public static RectangleF ChangeCoordSystem(this RectangleF rect)
        {
            return new RectangleF(rect.X, Constants.MapHeight * Constants.MapScale - rect.Y - rect.Height, rect.Width, rect.Height);
        }

        public static Rectangle ChangeCoordSystem(this Rectangle rect)
        {
            return new Rectangle(rect.X, Constants.MapHeight * Constants.MapScale - rect.Y - rect.Height, rect.Width, rect.Height);
        }

        public static Point ChangeCoordSystem(this Point p)
        {
            return new Point(p.X, Constants.MapHeight * Constants.MapScale - p.Y);
        }
    }
}