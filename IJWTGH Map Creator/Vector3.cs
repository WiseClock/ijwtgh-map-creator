﻿namespace IJWTGH_Map_Creator
{
    public class Vector3
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector3(float x, float y)
            : this(x, 0, y)
        {}

        public void TransformTo(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public void TransformTo(float x, float z)
        {
            TransformTo(x, 0, z);
        }
    }
}