﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace IJWTGH_Map_Creator
{
    public sealed partial class MainForm : Form
    {
        private bool _drawGrids = true;
        private readonly Pen _penGridThin = new Pen(Color.FromArgb(50, Color.DarkGray), 0.5f);
        private readonly Pen _penGridThick = new Pen(Color.FromArgb(50, Color.Red), 1.5f);
        private readonly ContextMenu _mapContextMenu = new ContextMenu();
        private readonly BindingList<Entity> _entityList = new BindingList<Entity>();

        private Keys _keyCode = Keys.None;
        private Point _recordedRightClickPos = Point.Empty;
        private int _selectedEntityIndex = -1;
        private bool _isMapMouseDown;

        public MainForm()
        {
            InitializeComponent();
            SetStyle(
                ControlStyles.AllPaintingInWmPaint |
                ControlStyles.UserPaint |
                ControlStyles.DoubleBuffer |
                ControlStyles.OptimizedDoubleBuffer,
                true);
            UpdateStyles();
            DoubleBuffered = true;
            Load += FormLoad;

            canvas.Size = new Size(Constants.MapWidth * Constants.MapScale, Constants.MapHeight * Constants.MapScale);
            canvas.Location = new Point(0, 0);
            canvas.Paint += Render;
            canvas.MouseUp += OnMapMouseUp;
            canvas.MouseDown += OnMapMouseDown;
            canvas.MouseMove += OnMapMouseMove;

            KeyDown += OnKeyDown;
            KeyPress += OnKeyPress;
            KeyUp += OnKeyUp;
            
            _mapContextMenu.MenuItems.Add("&Add Entity", OnMapMenuAddClick);
            _mapContextMenu.MenuItems.Add("&Delete Entity", OnMapMenuDeleteClick);
            _mapContextMenu.MenuItems.Add("-");
            _mapContextMenu.MenuItems.Add("Add &Renderable", OnMapMenuRenderableClick);
            _mapContextMenu.MenuItems.Add("Add &Physics", OnMapMenuPhysicsClick);
            _mapContextMenu.Popup += OnMapMenuPopup;

            entityListBox.ValueMember = "Name";
            entityListBox.DisplayMember = "Name";
            entityListBox.DataSource = _entityList;
        }

        private void OnMapMenuPopup(object sender, EventArgs e)
        {
            _mapContextMenu.MenuItems[1].Enabled = _selectedEntityIndex != -1;

            // todo
            _mapContextMenu.MenuItems[3].Enabled = false;
            _mapContextMenu.MenuItems[4].Enabled = false;
        }

        private void OnMapMenuRenderableClick(object sender, EventArgs e)
        {

        }

        private void OnMapMenuPhysicsClick(object sender, EventArgs e)
        {

        }

        private void OnMapMenuAddClick(object sender, EventArgs e)
        {
            if (_recordedRightClickPos != Point.Empty)
            {
                Point convertedPos = _recordedRightClickPos.ChangeCoordSystem();

                double posX = convertedPos.X / (double) Constants.MapScale;
                double posY = convertedPos.Y / (double) Constants.MapScale;

                if (_drawGrids)
                {
                    posX = Math.Round(posX * 4) / 4;
                    posY = Math.Round(posY * 4) / 4;
                }
                
                _entityList.Add(new Entity("Test-" + Guid.NewGuid(), new Vector3((float) posX, (float) posY), new Vector3(0.5f, 0.5f, 0.5f)));
                _selectedEntityIndex = _entityList.Count - 1;
                entityListBox.SelectedIndex = _selectedEntityIndex;
                canvas.Refresh();
            }
        }

        private void OnMapMenuDeleteClick(object sender, EventArgs e)
        {
            if (_selectedEntityIndex != -1)
            {
                _entityList.RemoveAt(_selectedEntityIndex);
                _selectedEntityIndex = -1;
                entityListBox.SelectedIndex = _selectedEntityIndex;
                canvas.Refresh();
            }
        }

        private void FormLoad(object sender, EventArgs e)
        {
            canvasFrame.VerticalScroll.Value = canvasFrame.VerticalScroll.Maximum;
            canvasFrame.PerformLayout();
        }

        private void OnMapMouseDown(object sender, MouseEventArgs e)
        {
            for (int i = _entityList.Count - 1; i >= 0; --i)
            {
                if (_entityList[i].ContainsPoint(e.Location))
                {
                    _isMapMouseDown = e.Button == MouseButtons.Left;
                    _selectedEntityIndex = i;
                    entityListBox.SelectedIndex = _selectedEntityIndex;
                    canvas.Refresh();
                    return;
                }
            }

            _selectedEntityIndex = -1;
            entityListBox.SelectedIndex = _selectedEntityIndex;
            canvas.Refresh();
        }

        private void OnMapMouseUp(object sender, MouseEventArgs e)
        {
            _isMapMouseDown = false;

            if (e.Button == MouseButtons.Right)
            {
                _recordedRightClickPos = e.Location;
                _mapContextMenu.Show((Control) sender, _recordedRightClickPos);
            }
        }

        private void OnMapMouseMove(object sender, MouseEventArgs e)
        {
            if (_isMapMouseDown && _selectedEntityIndex != -1)
            {
                Point convertedPos = e.Location.ChangeCoordSystem();

                double posX = convertedPos.X / (double)Constants.MapScale;
                double posY = convertedPos.Y / (double)Constants.MapScale;

                if (_drawGrids)
                {
                    posX = Math.Round(posX * 4) / 4;
                    posY = Math.Round(posY * 4) / 4;
                }

                Entity entity = _entityList[_selectedEntityIndex];
                entity.TransformTo((float) posX, (float) posY);
                canvas.Refresh();
            }
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            _keyCode = e.KeyCode;
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            _keyCode = Keys.None;
        }

        private void OnKeyPress(object sender, KeyPressEventArgs e)
        {
            if (_keyCode != Keys.None && _keyCode == Keys.G)
            {
                _drawGrids = !_drawGrids;
                canvas.Refresh();
            }
        }

        private void Render(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            g.FillRectangle(Brushes.White, 0, 0, Constants.MapWidth * Constants.MapScale, Constants.MapHeight * Constants.MapScale);

            if (_drawGrids)
            {
                // vertical lines
                for (int i = 0; i < Constants.MapWidth; ++i)
                {
                    float firstLine = (i + 0.25f) * Constants.MapScale;
                    float secondLine = (i + 0.5f) * Constants.MapScale;
                    float thirdLine = (i + 0.75f) * Constants.MapScale;
                    float lastLine = (i + 1) * Constants.MapScale;
                    g.DrawLine(_penGridThin, firstLine, 0, firstLine, Constants.MapHeight * Constants.MapScale);
                    g.DrawLine(_penGridThin, secondLine, 0, secondLine, Constants.MapHeight * Constants.MapScale);
                    g.DrawLine(_penGridThin, thirdLine, 0, thirdLine, Constants.MapHeight * Constants.MapScale);
                    g.DrawLine(_penGridThick, lastLine, 0, lastLine, Constants.MapHeight * Constants.MapScale);
                }

                // horizontal lines
                for (int i = 0; i < Constants.MapHeight; ++i)
                {
                    float firstLine = (i + 0.25f) * Constants.MapScale;
                    float secondLine = (i + 0.5f) * Constants.MapScale;
                    float thirdLine = (i + 0.75f) * Constants.MapScale;
                    float lastLine = (i + 1) * Constants.MapScale;
                    g.DrawLine(_penGridThin, 0, firstLine, Constants.MapHeight * Constants.MapScale, firstLine);
                    g.DrawLine(_penGridThin, 0, secondLine, Constants.MapHeight * Constants.MapScale, secondLine);
                    g.DrawLine(_penGridThin, 0, thirdLine, Constants.MapHeight * Constants.MapScale, thirdLine);
                    g.DrawLine(_penGridThick, 0, lastLine, Constants.MapHeight * Constants.MapScale, lastLine);
                }
            }

            // draw entities
            for (int i = 0; i < _entityList.Count; ++i)
            {
                _entityList[i].Draw(g, i == _selectedEntityIndex);
            }
        }

        private void entityListBox_MouseDown(object sender, MouseEventArgs e)
        {
            _selectedEntityIndex = entityListBox.IndexFromPoint(e.Location);
            entityListBox.SelectedIndex = _selectedEntityIndex;
            canvas.Refresh();
        }
    }
}
